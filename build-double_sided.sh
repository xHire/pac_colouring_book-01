#!/bin/sh

cd `dirname $0`

echo "Building a double-sided Pepper & Carrot colouring book"
echo

echo ">>> downloading all missing artworks into tmp/"
sh download_artworks.sh tmp/

echo ">>> creating a temporary build/ directory"
mkdir -p build/
cd build/

echo ">>> building the title page (Scribus will open & close by itself)"
scribus -g -ns -py ../scribus_to_pdf.py -- ../title_page.sla.gz
mv ../title_page.pdf .

echo ">>> building the content"
sed -e "s|tmp|../tmp|" -e "s|{2017|{../2017|" ../content-double_sided.tex > content.tex
pdflatex content.tex

echo ">>> merging both outputs together"
mutool merge -o merged.pdf title_page.pdf content.pdf

echo ">>> transforming it to A4 format so that it can be folded to A5 after printing"
pdfbook merged.pdf --outfile ../pepperandcarrot-colouring_book-double_sided.pdf

echo ">>> cleaning"
cd ..
rm -rf build/

echo
echo "FINISHED! pepperandcarrot-colouring_book-double_sided.pdf is your new colouring book."
echo "Please enjoy it and share with everyone. :c)"
